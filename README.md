# ulog

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/yuuuhh/ulog.svg)](https://pkg.go.dev/gitlab.com/yuuuhh/ulog)

Package `ulog` provides base functionality for multi-level logging.

This package has no external dependency besides the standard library and default operating system tools.

## Settings

Set environment variable LOG_LEVEL=(NONE|ERROR|WARN|INFO|DEBUG|TRACE) for automatic logging level. Default: INFO

Also you can use functon SetLogLevel for level change.

## Example

```go
level := "TRACE"
ulog.SetLogLevel(level)
ulog.Debug(("Logging system initialized with LOG_LEVEL=%v\n", level))
```
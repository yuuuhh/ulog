package ulog

import (
	"log"
	"os"
)

type DummyWriter struct {
}

func (w *DummyWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}

var (
	Error *log.Logger
	Warn  *log.Logger
	Info  *log.Logger
	Debug *log.Logger
	Trace *log.Logger
)

func SetLogLevel(level string) {
	if level == "" {
		level = "INFO"
	}
	if level != "TRACE" && level != "DEBUG" && level != "INFO" && level != "WARNING" && level != "ERROR" {
		level = "INFO"
	}

	switch level {
	case "TRACE":
		Trace = log.New(os.Stdout, "TRACE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "DEBUG":
		Debug = log.New(os.Stdout, "DEBUG: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "INFO":
		Info = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "WARN":
		Warn = log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "ERROR":
		Error = log.New(os.Stdout, "ERROR: ", log.Ldate|log.Ltime|log.Lmsgprefix)
	}

	switch level {
	case "NONE":
		Error = log.New(&DummyWriter{}, "NONE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "ERROR":
		Warn = log.New(&DummyWriter{}, "NONE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "WARN":
		Info = log.New(&DummyWriter{}, "NONE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "INFO":
		Debug = log.New(&DummyWriter{}, "NONE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
		fallthrough
	case "DEBUG":
		Trace = log.New(&DummyWriter{}, "NONE: ", log.Ldate|log.Ltime|log.Lmsgprefix)
	}
	Trace.Printf("Logging system initialized with LOG_LEVEL=%v\n", level)
	Trace.Printf("Set log level with env.variable LOG_LEVEL=(NONE|ERROR|WARN|INFO|DEBUG|TRACE)\n")
}

func init() {
	SetLogLevel(os.Getenv("LOG_LEVEL"))
}